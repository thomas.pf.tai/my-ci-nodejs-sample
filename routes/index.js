const express = require('express');
const indexController = require('../controllers/index');

const router = express.Router();


/* GET home page. */

// function getHomePage(req, res) {
//   res.render('index', { title: 'Express shit' });
// }

function getHomePage(req, res, next) {
  const renderObject = {};
  renderObject.title = 'Express shit!';
  indexController.sum(13, 5, (error, results) => {
    if (error) return next(error);
    if (results) {
      renderObject.sum = results;
      res.render('index', renderObject);
      return results;
    }
    return undefined;
  });
}

router.get('/', getHomePage);

module.exports = router;
