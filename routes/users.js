const express = require('express');

const router = express.Router();

/* GET users listing. */
function getUserListing(req, res) {
  res.send('respond with a resource');
}
router.get('/', getUserListing);

module.exports = router;
