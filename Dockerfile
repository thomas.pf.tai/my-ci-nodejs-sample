FROM node:10-alpine

WORKDIR /app

ADD . /app

RUN npm install

EXPOSE 3000

RUN DEBUG=*

CMD ["npm", "start"]