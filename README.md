# Introduction
This repo contains a very simple **Node.js Express** application plus the scripts needed to deploy it to a remote linux host using **Docker**.

# How it does?
The `.gitlab-ci.yml` is the config file that tells GitLab-CI what to do. There are 3 stages:

1. lint & test - Use **ESlint** to ensure coding standard. Use **Mocha & Chai** for Unit testing
2. build - Build a **Docker** image and push to any registry you specify
3. deploy - SSH to your remote linux host (must have docker installed first), pull the image from the registry, deploy using `docker stack deploy`

The `./deploy` folder contains scripts that handle the deployment steps in detail.

# Note:
After cloning this repo, don't forget to add the following Environment Variables under Settings --> CI/CD:

| Variable key         | Variable value  |
| ---------------------| -------------------------------------------------------------------------- |
| CI_DEPLOY_USER       | the user name you use to login the remote linux host                       |
| CI_REGISTRY          | the docker image registry, for dockerhub use docker.io                     |
| CI_REGISTRY_PASSWORD | the password used for logging in the image registry                        |
| CI_REGISTRY_USER     | the user name for logging in the image registry                            |
| DEPLOY_SERVERS       | the IPs of the servers you want to deploy to. Separated by comma           |
| SSH_PRIVATE_KEY      | the content of the xxxxx.pem file you use to SSH into the remote linux host|

# Result:
After deployment, the nodejs app runs in a container than forwards TCP 80 to 3000 of the app.

You can access it by: http://\<ip or domain\>

# Bonus: deploy without Docker
The `.gitlab-ci.yml` file also contains script block that deploys without using Docker. They are commented out.

It just copy neccessary files of the Node.js app to the remote host and then start it.

You won't be needing all those Environment Variables mentioned above, only the followings are needed:
* CI_DEPLOY_USER
* DEPLOY_SERVERS
* SSH_PRIVATE_KEY

There are a couple of required tools you need to install first:
1. Node.js 10 (duh!)
2. Forever (https://www.npmjs.com/package/forever) - To run the app in the background. Otherwise CI pipeline cannot finish.

# Result(without docker):
After deployment, the nodejs app runs on TCP port 3000.

You can access it by: http://\<ip or domain\>:3000
