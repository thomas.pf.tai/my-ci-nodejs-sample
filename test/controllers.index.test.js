const chai = require('chai');
const indexController = require('../controllers/index');

const should = chai.should();

process.env.NODE_ENV = 'test';

describe('controllers : index', function () {

  describe('sum()', function () {
    it('1+2=3', function (done) {
      indexController.sum(1, 2, function (err, total) {
        should.not.exist(err);
        total.should.eql(3);
        done();
      });
    });
    it('2+8=10', function (done) {
      indexController.sum(2, 8, function (err, total) {
        should.not.exist(err);
        total.should.eql(10);
        done();
      });
    });
    it('12+5=17', function (done) {
      indexController.sum(12, 5, function (err, total) {
        should.not.exist(err);
        total.should.eql(17);
        done();
      });
    });
  });

});