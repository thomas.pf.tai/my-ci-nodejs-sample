#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVERS
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"

  #Stop existing node.js app
  ssh $CI_DEPLOY_USER@${array[i]} "sudo pkill -9 node"

  #remove existing node.js app files
  ssh $CI_DEPLOY_USER@${array[i]} "rm -rf ./app && mkdir ~/app"

  #copy the necessary files of the Node.js app to remote host
  scp ./app.js $CI_DEPLOY_USER@${array[i]}:~/app/
  scp ./package-lock.json $CI_DEPLOY_USER@${array[i]}:~/app/
  scp ./package.json $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./bin $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./public $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./unit $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./controllers $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./routes $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./test $CI_DEPLOY_USER@${array[i]}:~/app/
  scp -r ./views $CI_DEPLOY_USER@${array[i]}:~/app/
  #start the node.js app
  ssh $CI_DEPLOY_USER@${array[i]} "npm install"
  ssh $CI_DEPLOY_USER@${array[i]} "cd ./app && npm install && forever start ./bin/www"
done