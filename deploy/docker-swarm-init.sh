#!/bin/bash
if [ "$(docker info | grep Swarm | sed 's/Swarm: //g')" == "inactive" ]; then
  docker swarm init
else
  echo "Docker already in swarm mode";
fi