#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVERS
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
  echo "Deploy project on server ${array[i]}"

  # Deploy a single container (minimalist approach)
  # TODO: actually you need to stop the existing container first, I just don't bother to invest more time.
  # ssh $CI_DEPLOY_USER@${array[i]} "sudo docker run -d -p 80:3000 $CI_REGISTRY_IMAGE"

  # Initialize docker swarm by a script
  # For this script to work you need to enable swap limit support at your EC2 instance first
  # Please take a look at: https://unix.stackexchange.com/questions/342735/docker-warning-no-swap-limit-support
  scp ./deploy/docker-swarm-init.sh $CI_DEPLOY_USER@${array[i]}:~
  ssh $CI_DEPLOY_USER@${array[i]} "sudo bash ./docker-swarm-init.sh"
  ssh $CI_DEPLOY_USER@${array[i]} "rm ./docker-swarm-init.sh"

  # Deploy a docker stack
  scp ./deploy/docker-compose.yml $CI_DEPLOY_USER@${array[i]}:~
  ssh $CI_DEPLOY_USER@${array[i]} "sudo docker stack deploy -c docker-compose.yml $CI_PROJECT_NAME"
  # keep the docker-compose.yml intentionally

  # Remove stopped containers and danggling images
  ssh $CI_DEPLOY_USER@${array[i]} "sudo docker system prune -f"
done